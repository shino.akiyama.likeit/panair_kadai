'''
Created on 2019/05/24

@author: akiyamashino
'''
if __name__ == '__main__':

    from datetime import datetime
    from itertools import groupby

    member_dict = {}  # 会員データを入れる空ディクショナリを用意する。

with open('members.txt', 'r', encoding='utf8') as f:
    for line1 in f.readlines():  # ファイルオブジェクトf を行単位で読み込み、line1へ代入しながらループする。
        line1 = line1.split('\t')  # タブ記号で区切って、リストにする。
        # 会員IDをキーにして、そのキーに対応する値である氏名を代入する。
        member_dict[line1[0]] = {'氏名': line1[1], '明細': []}

del member_dict['会員ID']  # 見出しのため削除する。

purchase_log_list = []  # 購入リストを入れる空リストを用意する。

with open('purchase_log.csv', 'r', encoding='utf8') as f2:
    for line1 in f2.readlines():  # ファイルオブジェクトf2 を行単位で読み込み、line1へ代入しながらループする。
        line1 = line1.split(',')  # カンマで区切って、リストにする。
        purchase_log_list.append([line1[0], line1[3], line1[4]])  # 会員ID, 金額, 日付をリストに追加する。

del purchase_log_list[0]  # 見出しのため削除する。

# 日付部分が、'2018 6/15 11:00' で扱いにくいので、抜き出す。
for i in range(len(purchase_log_list)):  # リストpurchase_log_listの中でループする。
    log_date = purchase_log_list[i][2][0:6]
    log_date = log_date.replace(' ', '-')
    log_date = log_date.replace('/', '')  # '/' を除去する。
    log_date = datetime.strptime(log_date, "%Y-%m")  # 日付型に変換する。
    purchase_log_list[i][2] = log_date

purchase_log_list.sort(key=lambda x:x[2])  # 日付順に並び変える。

for log1 in purchase_log_list:
    member_dict[log1[0]]['明細'].append(log1)

for key1 in member_dict:  # キーでループし、ループごとにkey1へ各キーを代入する。
    print(member_dict[key1]['氏名'])
    list1 = member_dict[key1]['明細']
    for date1, list2 in groupby(list1, key=lambda x:x[2]):
        print(date1.strftime('%Y{0}%m{1}').format(*'年月'), end=' ')
        print(sum([int(x[1]) for x in list2]), '円')
    print()  # 会員と会員の間を１行開ける。

